// Define the `letsapp` module
angular.module('letsapp', [
  // ...which depends on the `memberList` module
  'memberList'
]);