angular.module('memberList').
	component('memberList', {
		templateUrl: '/app/member-list/member-list.template.html',
		controller: function MemberListController($http) {
			var self = this;
			
			$http.get('http://api.letsjs.com/members').then(function(response) {
				self.members = response.data;
			});
		}
	});