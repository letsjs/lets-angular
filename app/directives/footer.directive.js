angular.module('app').directive("footer", function() {
  return {
    restrict: 'A',
    templateUrl: 'app/pages/footer/footer.view.html',
    scope: true,
    transclude : false,
    controller: 'FooterController'
  };
});