
angular.module('app').directive("header", function() {
  return {
    restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
    replace: true,
    templateUrl: 'app/pages/header/header.view.html',
    scope: {user: '='},
    transclude : false,
    controller: 'HeaderController'
  };
});

// Scroll directive
// Changes the Header upon scrolling
angular.module('app').directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
             if (this.pageYOffset >= 100) {
                element.find('#mainNav').addClass('affix');
             } else {
                element.find('#mainNav').removeClass('affix');
             }
        });
    };
});