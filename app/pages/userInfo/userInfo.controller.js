(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserInfoController', UserInfoController);

    UserInfoController.$inject = ['$scope', 'UserService', '$location', '$rootScope', 'FlashService'];
    function UserInfoController($scope, UserService, $location, $rootScope, FlashService) {
        var vm = this;

        vm.user = null;
        vm.userEdited = null;
        vm.allUsers = [];
        vm.deleteUser = deleteUser;

        vm.updateUser = updateUser;
        vm.password_verify = "";

        initController();

        function initController() {
            loadCurrentUser();
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                    vm.userEdited = user;

                    // Init form field variable
                    vm.userEdited.password = "";
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }

        function updateUser(user) {
            vm.dataLoading = true;
            if(vm.password_verify != "" && vm.userEdited.password != ""){
                if(vm.password_verify == user.password){
                    UserService.Update(user)
                        .then(function (response) {
                            if (response.success) {
                                FlashService.Success('Change successful', true);
                                $location.path('/');
                            } else {
                                FlashService.Error(response.message);
                                vm.dataLoading = false;
                            }
                        });
                    $location.path('/');
                }
                else{
                    FlashService.Error("Passwords Don't Match");
                    vm.dataLoading = false;
                }
            }else{
                UserService.Update(user)
                    .then(function (response) {
                        if (response.success) {
                            FlashService.Success('Change successful', true);
                            $location.path('/');
                        } else {
                            FlashService.Error(response.message);
                            vm.dataLoading = false;
                        }
                    });
                $location.path('/');
            }
        }
    }



})();