(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$location', 'AuthenticationService', 'UserService', '$rootScope', '$cookieStore'];
    function HomeController($location, AuthenticationService, UserService, $rootScope, $cookieStore) {
        var vm = this;

        vm.user = null;
        vm.allUsers = [];
        vm.deleteUser = deleteUser;
        vm.logout = logout;

        initController();

        function initController() {
            loadCurrentUser();
            loadAllUsers();            
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }

        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }

        function logout(){
            AuthenticationService.Logout( vm.user.username, function (response) {
                if (response.success) {
                    $location.path('/login');                        
                };
            });
        }
    }

})();