(function () {
    'use strict';
 
    angular
        .module('app')
        .controller('HeaderController', HeaderController);
 
    HeaderController.$inject = ['$scope', 'AuthenticationService', 'UserService', '$location', '$rootScope', 'FlashService'];
    function HeaderController($scope, AuthenticationService, UserService, $location, $rootScope, FlashService) {
        $scope.loggedIn = $rootScope.globals.loggedIn;
        $scope.currentUser = null;
        $scope.logout = logout;

        if($rootScope.globals.loggedIn){
            initController();
        }
        // Load CSS
        switch($location.path()){
            case "/":
                $scope.currPage = "home";
                break;
            case "/login":
                $scope.currPage = "login";
                break;
            case "/register":
                $scope.currPage = "login";
                break;
            case "/about":
                $scope.currPage = "login";
                break;
            case "/editUserInfo":
                $scope.currPage = "home";
                break;
            default:

        }

        function initController(){
            loadCurrentUser();
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    $scope.currentUser = user;
                });
        }

        function logout(){
            AuthenticationService.Logout( $scope.currentUser.username, function (response) {
                if (response.success) {
                    $location.path('/login');                        
                };
            });
        }
    } 
})();