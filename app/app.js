(function () {
    'use strict';

    var app = angular
        .module('app', ['ngRoute', 'ngCookies', 'angular.css.injector'])
        .config(config)
        .run(run);


    // Routes
    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'app/pages/home/home.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'app/pages/login/login.view.html',
                controllerAs: 'vm'
            })

            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'app/pages/register/register.view.html',
                controllerAs: 'vm'
            })

            .when('/about', {
                controller: 'AboutUsController',
                templateUrl: 'app/pages/about/about.view.html',
                controllerAs: 'vm'
            })

            .when('/editUserInfo', {
                controller: 'UserInfoController',
                templateUrl: 'app/pages/userInfo/userInfo.view.html',
                controllerAs: 'vm'
            })

            .otherwise({ redirectTo: '/about' });
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
    function run($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/about']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }
})();